from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'index.html')

def experiences(request):
    return render(request, 'experiences.html')

def education(request):
    return render(request, 'education.html')

def contact(request):
    return render(request, 'contact.html')

def regist(request):
    return render(request, 'regist.html')
