from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name = 'home'),
    path('experiences/', views.experiences, name = 'experiences'),
    path('education/', views.education, name = 'education'),
    path('contact/', views.contact, name = 'contact'),
    path('regist/', views.regist, name = 'experiences'),
]
