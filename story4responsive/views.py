from django.shortcuts import render
from .forms import *
from django.http import HttpResponseRedirect

# Create your views here.
def home(request):
    response = {'title':'Home'}
    return render(request, 'index.html', response)

def experiences(request):
    response = {'title':'Experiences'}
    return render(request, 'experiences.html', response)

def regist(request):
    response = {'title':'Register'}
    return render(request, 'register.html', response)

def activity_list(request):
    activity = Activity.objects.all().order_by("-date")
    response = {'activity' : activity, 'title' : 'List'}
    return render(request, 'list.html', response)

def create_activity(request):
    if request.method == "POST":
        form = ActivityForm(request.POST)
        if form.is_valid():
            activity = Activity()
            activity.title = form.cleaned_data['title']
            activity.date = form.cleaned_data['date']
            activity.place = form.cleaned_data['place']
            activity.category = form.cleaned_data['category']
            activity.save()
        return HttpResponseRedirect('/list')
    else:
        form = ActivityForm()
        response = {'form' : form, 'title' : 'Activity'}
        return render(request, 'forms.html', response)

def delete_all(request):
    Activity.objects.all().delete()
    return render(request, 'list.html')
