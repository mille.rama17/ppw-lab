from django.apps import AppConfig


class Story4ResponsiveConfig(AppConfig):
    name = 'story4responsive'
