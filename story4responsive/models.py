from django.db import models

# Create your models here.
class Activity(models.Model):
    title = models.CharField(max_length=140)
    date = models.DateTimeField()
    place = models.CharField(max_length=140)
    category = models.CharField(max_length=140)

    def __str__(self):
        return self.title
