from django import forms
from .models import Activity

class ActivityForm(forms.Form):
    title = forms.CharField(label="Activity Name", max_length = 30, widget = forms.TextInput(
        attrs = {
                'class':'form-group col-md-12',
                'placeholder':'Activity',
                'required':True,
        }
    ))

    date = forms.DateTimeField(label="Date", widget = forms.DateTimeInput(
        attrs = {
                'type':'date',
                'class':'form-group col-md-12',
                'placeholder':'date',
                'required':True,
        }
    ))

    place = forms.CharField(label="Location", max_length = 30, widget = forms.TextInput(
        attrs = {
                'class':'form-group col-md-12',
                'placeholder':'Location',
                'required':True,
        }
    ))

    category = forms.CharField(label="Category", max_length = 30, widget = forms.TextInput(
        attrs = {
                'class':'form-group col-md-12',
                'placeholder':'Category',
                'required':True,
        }
    ))
