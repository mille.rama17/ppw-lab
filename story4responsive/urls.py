from django.conf.urls import url, include
from django.urls import path
from . import views
from story4responsive.models import Activity


urlpatterns = [
    path('', views.home, name = 'home'),
    path('experiences/', views.experiences, name = 'experiences'),
    path('register/', views.regist, name = 'register'),
    path('forms/', views.create_activity, name = 'create'),
    path('list/', views.activity_list, name = 'list'),
    path('delete/', views.delete_all, name = 'delete_all')
]
